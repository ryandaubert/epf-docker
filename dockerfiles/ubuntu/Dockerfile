FROM ubuntu:22.04

ARG eggplant_flavour="Ubuntu22"
ARG eggplant_version=23.4.1
ARG eggplant_archive=Eggplant_${eggplant_flavour}_${eggplant_version}
ARG eggplant_url="https://assets.eggplantsoftware.com/Linux/${eggplant_archive}.tgz"
ARG USERNAME=epf-docker
ARG USER_UID=1000
ARG USER_GID=$USER_UID

LABEL "org.opencontainers.image.authors"="Frederik Carlier <frederik.carlier@keysight.com>, Sebastien Morgant <sebastien.morgant@keysight.com>"
LABEL "org.opencontainers.image.url"="https://eggplantsoftware.com/"
LABEL "org.opencontainers.image.documentation"="https://docs.eggplantsoftware.com/"
LABEL "org.opencontainers.image.vendor"="Keysight Technologies"
LABEL "org.opencontainers.image.title"="Eggplant functional with eggdrive"

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ="Etc/UTC"

WORKDIR /tmp

RUN apt update \
&& apt install -y curl \
&& curl $eggplant_url --output ${eggplant_archive}.tgz \
&& tar xvzf ${eggplant_archive}.tgz \
&& rm ${eggplant_archive}.tgz \
&& apt install -y ./Eggplant_${eggplant_flavour}/Eggplant${eggplant_version}.deb \
&& rm -rf Eggplant_${eggplant_flavour} \
&& rm -rf /var/lib/apt/lists/*

RUN groupadd --gid $USER_GID $USERNAME \
&& useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
&& mkdir -p /home/$USERNAME \
&& chown ${USERNAME}:${USERNAME} /home/${USERNAME}

USER ${USERNAME}

WORKDIR /home/${USERNAME}

# Set the backend to libgnustep-back-headless to remove the dependency on X;
RUN /usr/local/bin/defaults write Eggplant GSBackend libgnustep-headless \
&& /usr/local/bin/defaults write Eggplant BonjourDiscoveryEnabled false \
&& /usr/local/bin/defaults write Eggplant AcceptEULA YES \
&& /usr/local/bin/defaults write Eggplant AcceptPrivacyAgreement YES
